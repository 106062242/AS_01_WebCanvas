var canvas; 
var context;
var saveData;
var beginX;
var beginY;
var startx;
var w;
var step = -1;
var r,g,b;

var pan = false,draw_pan = false;
var rec = false,rectan = false;
var cir = false,cycle = false;
var tri = false,trian = false;
var Line = false,linedraw = false;
var write = false;
var era = false;
var eraser = false;
var fill = false;

var recentWords = [];
var undoList = [];
var canvasHistory = [];

window.onload = function(){
    canvas = document.getElementById("myCanvas");
    context = canvas.getContext("2d");
}

function reset(){
    draw_pan = false;
    rec = false;
    cir = false;
    tri = false;
    Line = false;
    write = false;
    era = false;
}

function dosomething(){
    step++;
    if(step < canvasHistory.length){
        canvasHistory.length = step;
    }
    canvasHistory.push(canvas.toDataURL());
}

function prev(){
    if(step>=0){
        step--;
        context.clearRect(0,0,canvas.width,canvas.height);
        var canpic = new Image();
        canpic.src = canvasHistory[step];
        canpic.addEventListener('load',()=>{
            context.drawImage(canpic,0,0);
        });
    }else{
        console.log('不能再往前了 哥哥!');
    }
}

function next(){
    if(step < canvasHistory.length-1){
        step++;
        var canpic = new Image();
        canpic.src = canvasHistory[step];
        canpic.addEventListener('load',()=>{
            context.clearRect(0,0,canvas.width,canvas.height);
            context.drawImage(canpic,0,0);
        });
    }else{
        console.log('不能再往後了 哥哥!');
    }
}

function mousedown(){
    saveData = context.getImageData(0,0,canvas.width,canvas.height);
    beginX = event.offsetX;
    beginY = event.offsetY;
    startx = event.offsetX;
    r = document.getElementById("red").value;
    g = document.getElementById("green").value;
    b = document.getElementById("blue").value;
    w = document.getElementById("wid").value;
    recentWords = [];
    context.moveTo(event.offsetX,event.offsetY);
    pan = true;
    eraser = true;
    linedraw = true;
    rectan = true;
    cycle = true;
    trian = true;
    context.beginPath();
    if(era){
        context.save();
        context.globalCompositeOperation = 'destination-out';
        context.arc(beginX,beginY,w,0,2*Math.PI,false);
        context.fill();
        context.restore();
        dosomething();
    }
}

function mousemove(){
    context.lineWidth = w;
    context.strokeStyle = "rgb("+r+","+g+","+b+")";
    context.fillStyle = "rgb("+r+","+g+","+b+")";
    if(draw_pan){
        if(pan){
            context.lineTo(event.offsetX,event.offsetY);
            context.stroke();
        }
    }else if(era){
        if(eraser){
            context.beginPath();
            context.save();
            context.globalCompositeOperation = 'destination-out';
            context.arc(event.offsetX,event.offsetY,w,0,2*Math.PI,false);
            context.fill();
            context.restore();
        }
    }else if(Line){
        if(linedraw){
            context.putImageData(saveData,0,0);
            context.beginPath();
            context.moveTo(beginX,beginY);
            context.lineTo(event.offsetX,event.offsetY);
            context.stroke();
        }
    }else if(rec){
        if(rectan){
            context.putImageData(saveData,0,0);
            if(fill) 
                context.fillRect(beginX,beginY,event.offsetX-beginX,event.offsetY-beginY);
            else
                context.strokeRect(beginX,beginY,event.offsetX-beginX,event.offsetY-beginY);
        }
    }else if(cir){
        if(cycle){
            context.putImageData(saveData,0,0);
            context.beginPath();
            var x=Math.abs(event.offsetX-beginX);
            var y=Math.abs(event.offsetY-beginY);
            var rad = Math.sqrt(Math.pow(x,2)+Math.pow(y,2));
            context.arc(beginX, beginY, rad, 0, 2 * Math.PI);
            context.stroke();
            if(fill) context.fill();
        }
    }else if(tri){
        if(trian){
            context.putImageData(saveData,0,0);
            context.beginPath();
            var x=event.offsetX-beginX;
            context.moveTo(beginX,beginY);
            if(fill){  
                context.lineTo(event.offsetX,event.offsetY);
                context.lineTo(event.offsetX-2*x,event.offsetY);
                context.fill();
            }else{
                context.lineTo(event.offsetX,event.offsetY);
                context.stroke();
                context.lineTo(event.offsetX-2*x,event.offsetY);
                context.stroke();
                context.lineTo(beginX,beginY);
                context.stroke();
            }
        }
    }
}

function mouseup(){
    pan = false;
    eraser = false;
    linedraw = false;
    rectan = false;
    cycle = false;
    trian = false;
    if(draw_pan || era || rec || cir || tri || Line) dosomething();
    context.closePath();
}

function saveState(){
    undoList.push(canvas.toDataURL());
}

function undo(){
    undoList.pop();
    var imgData = undoList[undoList.length-1];
    var image = new Image();

    image.src = imgData;
    image.onload = function () {
        context.clearRect(0,0,canvas.width,canvas.height);
        context.drawImage(image,0,0,canvas.width,canvas.height,0,0,canvas.width,canvas.height);
    };
} 

document.addEventListener("keydown",function (e){
    context.font = "16px Arial";
    if(write){
        if(e.keyCode === 8){
            undo();
            var recentWord = recentWords[recentWords.length-1];
            beginX -= context.measureText(recentWord).width;
            recentWords.pop();
        }else if(e.keyCode === 13){
            beginX = startx;
            beginY += 20;
        }else{
            context.fillText(e.key,beginX,beginY);
            beginX += context.measureText(e.key).width;
            saveState();
            recentWords.push(e.key);
        }
        dosomething();
    }
},false);

function draw(){
    reset();
    draw_pan = true;
}

function rectangle(){
    reset();
    rec = true;
}

function circle(){
    reset();
    cir = true;
}

function triangle(){
    reset();
    tri = true;
}

function line(){
    reset();
    Line = true;
}

function text(){
    reset();
    write = true;
}

function fill_or_stroke(){
    fill = !fill;
}

function erase(){
    reset();
    era = true;
}

function upload(input){
    var file = input.files[0];
    var src = URL.createObjectURL(file);
    var img = new Image();
    img.src = src;
    img.onload = function(){
        context.drawImage(this,0,0,canvas.width,canvas.height);
    };
}

function save(){
    var link=document.getElementById("download");
    link.download="image.jpg";
    link.herf=canvas.toDataURL("image/jpeg");
    link.click();
}