as_01_webcanvas

HTML
===
一開始最上面是選顏色,再來是選擇筆的粗細,再來是canvas,最後是各個不同功能的按鈕。

CSS
===
最上面是背景,再來是對canvas的一些設定,再來就是按鈕的動畫,有四個不同的方向,對每邊做一段動畫
,最後再加上延遲時間,按鈕的動畫就完成了。

JS
===
在一開始先取得    
    document.getElementById("myCanvas");
    canvas.getContext("2d");
這兩個物件。
### reset()
重設各種變數,主要是讓功能在切換時不會有一次出現兩種功能的情況。

### dosomething()
在做完一個動作後呼叫,把現在canvas的狀態儲存下來,讓以後undo和redo功能使用。

### prev()
在按下REDO按鈕後呼叫,就是把上一個存在array裡的canvas的狀態丟出來。

### next()
在按下UNDO按鈕後呼叫,就是把下一個存在array裡的canvas的狀態丟出來。

### mousedown()
在按下滑鼠後呼叫,初始化各種變數,主要是讓各種按鈕的功能開啟,至於是按了什麼按鈕,會在下一個function判斷。

### mousemove()
在滑鼠移動時呼叫,主要判斷現在是哪個按鈕,並且實現畫圖的功能。

### mouseup()
結束畫圖功能,並且在畫完圖後在這裡呼叫dosomething()。

### saveState()
儲存目前的canvas,用於鍵盤backspace功能

### undo()
在鍵盤按下backspace時呼叫,把儲存在array裡的上一個canvas畫出來。

### upload(input)
在按下上傳檔案時呼叫

### save()
要下載檔案時呼叫

### 剩下
剩下的function主要都是用來開啟按鍵功能的,要做的就只有把目前的功能先關掉(呼叫reset();),再把對應的按鍵變數設成true。

